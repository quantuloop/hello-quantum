# "Hello, Quantum!"

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/quantuloop%2Fhello-quantum/HEAD?labpath=hello_quantum.ipynb)

In classical computing, a **"Hello, World!"** program is a small piece of code that illustrates the syntax of a programming language with a program that prints "Hello, World!" on the screen. In the Jupyter Notebook [hello_quantum.ipynb](hello_quantum.ipynb), we present a quantum "Hello, World!" (or **"Hello, Quantum!"**) implemented in the quantum programming language Ket.

The typical "Hello, World!" program displays the message "Hello, World!". However, there is nothing quantum about it. So instead, for our "Hello, Quantum!", we'll implement a program that explores three quantum properties: superposition, entanglement, and measurement.

You can run this Notebook on your computer by clicking on the Binder badge at the top of this README.
