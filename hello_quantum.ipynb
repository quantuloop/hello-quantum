{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# \"Hello, Quantum!\"\n",
    "\n",
    "[![Open In Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/drive/1hRQ2muwvhrVTSzXEx2WeJfh4WsIkteph?usp=sharing)\n",
    "[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/quantuloop%2Fhello-quantum/HEAD?labpath=hello_quantum.ipynb)\n",
    "\n",
    "In classical computing, a **\"Hello, World!\"** program is a small piece of code that illustrates the syntax of a language with a program that prints \"Hello, World!\" on the screen.\n",
    "In this Notebook, we present a quantum \"Hello, World!\" (or **\"Hello, Quantum!\"**) implemented in the quantum programming language Ket.\n",
    "\n",
    "The typical \"Hello, World!\" program displays the message \"Hello, World!\". However, there is nothing quantum about it. So instead, for our \"Hello, Quantum!\", we'll implement a program that explores three quantum properties: superposition, entanglement, and measurement."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Preamble\n",
    "Ket is an embedded programming language that introduces the ease of Python to quantum programming. So before the quantum code, you must import the Ket module into Python.\n",
    "\n",
    "You can run this Notebook on your computer by clicking on the **Colab** (preferable) or **Binder** badge at the top of the Notebook. With the Notebook ready, you can run a cell by selecting it and using the shortcut `Ctrl+Enter` (or `⌘+Enter` on Mac).\n",
    "\n",
    "Go ahead! Run the cell below to import Ket's functionalities in the Notebook. You should see the Ket version printed below the cell."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ket import *\n",
    "!ket --version"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Bell State\n",
    "\n",
    "For our \"Hello Quantum!\", we will put two qubits (quantum bits) in superposition and entangle them, creating a Bell state. \n",
    "\n",
    "The following section presents how to create the superposition and entangle the qubits in the Bell state. Before, let's see what the Bell state looks like:\n",
    "$$\\left|\\beta_{00}\\right> = \\frac{\\left|0_\\psi0_\\varphi\\right>+\\left|1_\\psi1_\\varphi\\right>}{\\sqrt{2}}$$\n",
    "\n",
    "The state above represents two entangled qubits, $\\left|\\psi\\right>$ and $\\left|\\varphi\\right>$, in a superposition of states $\\left|0\\right>$ and $\\left|1\\right>$.\n",
    "\n",
    "We cannot efficiently extract a quantum superposition from a quantum computer, so we need to measure the qubits to get any classical information from the quantum computation. Although, this operation collapses the quantum state destroying the superposition.\n",
    "\n",
    "In this particular quantum state, there is an equal probability of measuring 0 or 1 in qubits $\\left|\\psi\\right>$ and $\\left|\\varphi\\right>$. Furthermore, as this is an entangled state, the measurement of one qubit influences the other. Therefore, for this Bell state, the measurement result of $\\left|\\psi\\right>$ and $\\left|\\varphi\\right>$ will always be the same. The quantum state will collapse at $\\left|00\\right>$ if the measurements return 0 and $\\left|11\\right>$ if they return 1."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Quantum Programming\n",
    "\n",
    "To program our \"Hello, Quantum!\", first, let's implement the function `bell` that takes two qubits (`a` and `b`) in the state $\\left|00\\right>$ and prepares them in the Bell state. This implementation requires two quantum operations: a Hadamard gate and a CNOT gate.\n",
    "\n",
    "If qubits `a=`$\\left|\\psi\\right>$ and `b=`$\\left|\\varphi\\right>$ are in the state $\\left|0_\\psi0_\\varphi\\right>$, the evolution of their quantum state in the `bell` function is:\n",
    "\n",
    "1. $$H_\\psi\\left|0_\\psi0_\\varphi\\right> = \\frac{\\left|0_\\psi0_\\varphi\\right>+\\left|1_\\psi0_\\varphi\\right>}{\\sqrt{2}}$$\n",
    "2. $$\\text{CNOT}_{\\psi,\\varphi}\\left[\\frac{\\left|0_\\psi0_\\varphi\\right>+\\left|1_\\psi0_\\varphi\\right>}{\\sqrt{2}}\\right] = \\frac{\\left|0_\\psi0_\\varphi\\right>+\\left|1_\\psi1_\\varphi\\right>}{\\sqrt{2}}$$\n",
    "\n",
    "The first step applies a quantum operation known as the Hadamard gate on qubit $\\left|\\psi\\right>$. We use the Hadamard gate to create an equal superposition of the state $\\left|0\\right>$ and $\\left|1\\right>$ in qubit $\\left|\\psi\\right>$. Before this operation, qubits $\\left|\\psi\\right>$ and $\\left|\\varphi\\right>$ were neither superposition nor entangled.\n",
    "\n",
    "Step 1 doesn't entangle the qubits $\\left|\\psi\\right>$ and $\\left|\\varphi\\right>$. Therefore we can represent their state alternatively as\n",
    "$$H_\\psi(\\left|0_\\psi\\right>\\otimes\\left|0_\\varphi\\right>) = \\frac{\\left|0_\\psi\\right>+\\left|1_\\psi\\right>}{\\sqrt{2}}\\otimes\\left|0_\\varphi\\right>.$$\n",
    "\n",
    "The second step applies a controlled quantum operation known as the CNOT gate. This operation takes a _control_ qubit and a _target_ qubit, flipping the _target_ qubit ($\\left|0\\right>\\leftrightarrow\\left|1\\right>$) if the _control_ qubit is on state $\\left|1\\right>$. After the CNOT gate application, the state of qubit $\\left|\\varphi\\right>$ (_target_) is conditioned to the state of qubit $\\left|\\psi\\right>$ (_control_), entangling $\\left|\\psi\\right>$ and $\\left|\\varphi\\right>$ since $\\left|\\psi\\right>$ is in a superposition of states $\\left|0\\right>$ and $\\left|1\\right>$.\n",
    "\n",
    "The final state is a superposition of states $\\left|00\\right>$ and $\\left|11\\right>$, where the qubit $\\left|\\varphi\\right>$ is in a superposition of flipped and not flipped. Since the state of qubit $\\left|\\varphi\\right>$ depends on the state of $\\left|\\psi\\right>$, we cannot describe them separated by a $\\otimes$.\n",
    "\n",
    "\n",
    "The cell below defines the `bell` function and prints a Bell state. As mentioned before, we cannot get the superposition from a quantum computer. However, for this \"Hello, Quantum!\", we use a simulator for the quantum execution, which allows us to print the state. \n",
    "\n",
    "Go ahead! Run the next cell.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def bell(a, b):\n",
    "    H(a)\n",
    "    cnot(a, b)\n",
    "\n",
    "q = quant(2)\n",
    "bell(q[0], q[1])\n",
    "print(dump(q).show())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With the `bell` function defined, let's (1) allocate two qubits, (2) pass them to the `bell` function, and (3) measure them.\n",
    "\n",
    "1. Ket stores the qubits in a list-like structure named `quant`. The `quant`'s constructor allocates a given number of qubits. For the \"Hello, Quantum!\", we first assign two qubits to the variable `q`.\n",
    "2. Next, we need to pass the qubits to the `bell` function. We can separate the qubits `a` and `b` using square brackets to index the first and second qubits of `q`. Note that the indexation starts at 0.\n",
    "3. And finally, we measure the qubits from `q`, reading the result from the attribute `value` and printing it on the screen. \n",
    "\n",
    "Since the Bell state is a superposition of the states $\\left|00\\right>$ and $\\left|11\\right>$, the possible measurement outcomes are 0 (from state $\\left|00\\right>$) and 3 (from state $\\left|11\\right>$). Note that $11$ is 3 in binary.\n",
    "\n",
    "Now you can run the cell below to execute your first quantum program!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "q = quant(2)\n",
    "bell(q[0], q[1])\n",
    "print(\"Measurement result:\", measure(q).value)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The code above has a 50% chance of printing `Measurement result: 0` or `Measurement result: 3`.\n",
    "\n",
    "To illustrate the measurement probability, let's measure a Bell state 100 times and plot the results. Note that the possible measurement outcomes of arbitrary 2-qubits are $00$, $01$, $10$, $11$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import plotly.express as px\n",
    "import plotly.io as pio\n",
    "pio.renderers.default = 'svg'\n",
    "\n",
    "result = {i: 0 for i in range(4)}\n",
    "for i in range(100):\n",
    "    q = quant(2)\n",
    "    bell(q[0], q[1])\n",
    "    result[measure(q).value] += 1\n",
    "\n",
    "px.bar({'Result': map(lambda i: f'{i:02b}', result.keys()), 'Count': result.values()}, x='Result', y='Count')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Congratulations! You've run your first quantum program. \n",
    "\n",
    "To learn more about Ket and quantum programming, visit the Ket website https://quantumket.org."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.5"
  },
  "vscode": {
   "interpreter": {
    "hash": "168578aa468e5b45774fca3558af758994b2508f3fab0dac7630fe0374a3f27d"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
